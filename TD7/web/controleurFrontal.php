<?php

use App\Covoiturage\Controleur\ControleurUtilisateur;
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');


// On récupère l'action passée dans l'URL
if(!isset($_GET['controleur'])) $controleur = \App\Covoiturage\Lib\PreferenceControleur::lire();
else $controleur = $_GET['controleur'];

if(!isset($_GET['action'])) $action = 'afficherListe';
else $action = $_GET['action'];
$nomDeClasseControleur = "App\Covoiturage\Controleur\Controleur" . ucfirst($controleur);
$listefonction = get_class_methods($nomDeClasseControleur);
if(in_array($action, $listefonction) && class_exists($nomDeClasseControleur)) {
    // Appel de la méthode statique $action de ControleurUtilisateur
    $nomDeClasseControleur::$action();
}
else ControleurUtilisateur::afficherErreur("L'action n'est pas bonne");

?>