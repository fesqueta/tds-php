<!DOCTYPE html>
<html>

<body>
<?php
/** @var \App\Covoiturage\Modele\DataObject\Trajet $trajet */
    $tagfumeur = '';
    if($trajet->isNonFumeur()) $tagfumeur = 'non fumeur';
    else $tagfumeur = 'fumeur';

    echo "<p>
        Le trajet ". $trajet->getId()." $tagfumeur du {$trajet->getDate()->format("d/m/Y")} partira de 
        {$trajet->getDepart()} pour aller à {$trajet->getArrivee()} (conducteur : {$trajet->getConducteur()->getPrenom()} {$trajet->getConducteur()->getNom()}).
    </p>";
echo '<p> Supprimer le trajet  <a href="?action=supprimer&controleur=trajet&id='.rawurlencode($trajet->getId()).'">'. htmlspecialchars($trajet->getId()).'</a></p>';
echo '<p> Mettre à jour le trajet <a href="?action=afficherFormulaireMiseAJour&controleur=trajet&id='.rawurlencode($trajet->getId()).'">'. htmlspecialchars($trajet->getId()).'</a></p>';

//echo '<p> Supprimer l\'utilisateur  <a href="?action=supprimer&login='.rawurlencode($utilisateur->getLogin()).'">'. htmlspecialchars($utilisateur->getLogin()).'</a></p>';
//echo '<p> Mettre à jour l\'utilisateur  <a href="?action=afficherFormulaireMiseAJour&login='.rawurlencode($utilisateur->getLogin()).'">'. htmlspecialchars($utilisateur->getLogin()).'</a></p>';

?>
</body>
</html>