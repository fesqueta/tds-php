<!DOCTYPE html>
<html>
<?php
$controleur = \App\Covoiturage\Lib\PreferenceControleur::lire();

?>
<body>
<form method="get" action="controleurFrontal.php">
    <input type='hidden' name='action' value='enregistrerPreference'>
    <fieldset>
        <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur"
            <?php if ($controleur === 'utilisateur') echo 'checked'; ?>
        <label for="utilisateurId">Utilisateur</label>
        <input type="radio" id="trajetId" name="controleur_defaut" value="trajet"
            <?php if ($controleur === 'trajet') echo 'checked'; ?>
        <label for="trajetId">Trajet</label>
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>
</body>
</html>