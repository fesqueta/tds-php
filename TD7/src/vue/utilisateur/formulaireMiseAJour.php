<!DOCTYPE html>

<?php
/** @var string $login */

use App\Covoiturage\Modele\Repository\UtilisateurRepository;

$utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
$loginHTML = $utilisateur->getLogin();
$prenomHTML = $utilisateur->getPrenom();
$nomHTML = $utilisateur->getNom();
?>
<html>

<body>
<form method="get" action="controleurFrontal.php">
    <input type='hidden' name='action' value='mettreAJour'>
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login</label>
            <input readonly value="<?=htmlspecialchars($loginHTML)?>" class="InputAddOn-field" type="text" placeholder="Ex : leblancj" name="login" id="login_id" required>
        </p>


        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prenom&#42;</label>
             <input value="<?=htmlspecialchars($prenomHTML)?>" class="InputAddOn-field" type="text" placeholder="un prenom" name="prenom" id="prenom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom&#42;</label>
            <input value="<?=htmlspecialchars($nomHTML)?>" class="InputAddOn-field" type="text" placeholder="un nom" name="nom" id="nom_id" required/>
        </p>
        <p class="InputAddOn">
            <input class="InputAddOn-item" type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>

</body>
</html>