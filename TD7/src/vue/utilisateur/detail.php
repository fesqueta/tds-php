<!DOCTYPE html>
<html>

<body>
<?php
/** @var \App\Covoiturage\Modele\DataObject\Utilisateur $utilisateur */

    echo  htmlspecialchars($utilisateur->getNom()) . " " ;
    echo htmlspecialchars($utilisateur->getPrenom()). " " ;
    echo htmlspecialchars($utilisateur->getLogin()). " ";
    echo '<p> Supprimer l\'utilisateur  <a href="?action=supprimer&login='.rawurlencode($utilisateur->getLogin()).'">'. htmlspecialchars($utilisateur->getLogin()).'</a></p>';
    echo '<p> Mettre à jour l\'utilisateur  <a href="?action=afficherFormulaireMiseAJour&login='.rawurlencode($utilisateur->getLogin()).'">'. htmlspecialchars($utilisateur->getLogin()).'</a></p>';

?>
</body>
</html>