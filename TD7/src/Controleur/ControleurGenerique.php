<?php

namespace App\Covoiturage\Controleur;


use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique
{

    protected static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../../src/vue/$cheminVue"; // Charge la vue
    }
    public static function afficherFormulairePreference (): void{
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire de préférence","cheminCorpsVue" => "formulairePreference.php"]);

    }
    public static function enregistrerPreference  (): void{
        $controleur_defaut = $_GET["controleur_defaut"];
        PreferenceControleur::enregistrer($controleur_defaut);
        self::afficherVue("vueGenerale.php", ["titre" => "Préférence enregistrée","cheminCorpsVue" => "preferenceEnregistree.php"]);
    }
}