<?php

namespace App\Covoiturage\Modele\Repository;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use DateTime;
use PDO;

class TrajetRepository extends AbstractRepository {

    /**
     * @return Trajet[]
     */
    /*public static function recupererTrajets(): array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach ($pdoStatement as $trajetFormatTableau) {
            $trajets[] = TrajetRepository::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }*/

    protected function construireDepuisTableauSQL(array $trajetTableau): Trajet
    {
        $date = new DateTime($trajetTableau["date"]);
        $login = (new UtilisateurRepository())->recupererParClePrimaire($trajetTableau["conducteurLogin"]);

        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            $date,
            $trajetTableau["prix"],
            $login,
            $trajetTableau["nonFumeur"]
        );

        // Récupération des passagers pour ce trajet
        $passagers = self::recupererPassagers($trajet);

        // Stocker les passagers dans l'attribut $passagers
        $trajet->setPassagers($passagers ?? []);

        return $trajet;
    }

    /**
     * @return Utilisateur[]|null
     */
    static public function recupererPassagers(Trajet $trajet) : array {
        $sql = "SELECT passagerLogin FROM passager WHERE trajetId = :idTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "idTag" => $trajet->getId(),
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $temp = array();
        foreach($pdoStatement as $passagerFormatTableau) {
            /*var_dump($passagerFormatTableau);
            echo "<br><br>";*/
            $temp[] = (new UtilisateurRepository())->recupererParClePrimaire($passagerFormatTableau["passagerLogin"]);
        }

        return $temp;

    }
    protected function getNomTable(): string
    {
        return "trajet";
    }

    protected function getNomClePrimaire(): string
    {
        return "id";
    }

    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee","date", "prix","conducteurLogin","nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $trajet): array
    {
        /** @var Trajet $trajet */
        return array(
            "idTag" => $trajet->getId(),
            "departTag" => $trajet->getDepart(),
            "arriveeTag" => $trajet->getArrivee(),
            "dateTag" => $trajet->getDate()->format("Y-m-d"),
            "prixTag" => $trajet->getPrix(),
            "conducteurLoginTag" => $trajet->getConducteur()->getLogin(),
            "nonFumeurTag" => (int)$trajet->isNonFumeur(),
        );
    }


}