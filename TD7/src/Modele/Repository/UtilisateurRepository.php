<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;

class UtilisateurRepository extends AbstractRepository
{

    protected function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
        return new Utilisateur($utilisateurFormatTableau['loginBaseDeDonnees'],
            $utilisateurFormatTableau['nomBaseDeDonnees'],
            $utilisateurFormatTableau['prenomBaseDeDonnees']);
    }


    protected function getNomTable(): string
    {
        return "utilisateur";
    }

    protected function getNomClePrimaire(): string
    {
        return "loginBaseDeDonnees";
    }
    protected function getNomsColonnes(): array
    {
        return ["loginBaseDeDonnees", "nomBaseDeDonnees", "prenomBaseDeDonnees"];
    }
    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        /** @var Utilisateur $utilisateur */
        return array(
            "loginBaseDeDonneesTag" => $utilisateur->getLogin(),
            "nomBaseDeDonneesTag" => $utilisateur->getNom(),
            "prenomBaseDeDonneesTag" => $utilisateur->getPrenom(),
        );
    }



}