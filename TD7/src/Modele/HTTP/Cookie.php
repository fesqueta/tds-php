<?php

namespace App\Covoiturage\Modele\HTTP;

class Cookie
{
    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void
    {
        if ($dureeExpiration !== null) {
            $tempsExpiration = time() + $dureeExpiration;
        } else {
            $tempsExpiration = 0;
        }
        setcookie($cle, serialize($valeur), $tempsExpiration);
    }
    public static function lire(string $cle): mixed
    {
        return unserialize($_COOKIE[$cle]);
    }
    public static function contient($cle) : bool {
        return isset($_COOKIE[$cle]);
    }

    public static function supprimer($cle) : void {
        unset($_COOKIE[$cle]);
    }

}