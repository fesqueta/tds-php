<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\ModeleUtilisateur;
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs,"titre" => "Liste des utilisateurs","cheminCorpsVue" => "utilisateur/liste.php"]);
    }
    public static function afficherDetail() : void {
        $login = $_GET['login'];
        if($login == NULL || $login == ""){
            //require ('../vue/utilisateur/erreur.php');
            self::afficherVue('vueGenerale.php', ["titre" => "Page Erreur","cheminCorpsVue" => "utilisateur/erreur.php"]);

        return;}
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login']);
        if($utilisateur == NULL){
            //require ('../vue/utilisateur/erreur.php');
            self::afficherVue('vueGenerale.php', ["titre" => "Page Erreur","cheminCorpsVue" => "utilisateur/erreur.php"]);
        return;}
        //require ('../vue/utilisateur/detail.php');
        self::afficherVue('vueGenerale.php', ['utilisateur' => $utilisateur,"titre" => "Detail de l'utilisateur","cheminCorpsVue" => "utilisateur/detail.php"]);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__. "/../../src/vue/$cheminVue"; // Charge la vue
    }
    public static function afficherFormulaireCreation (): void {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire de création","cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }
    public static function creerDepuisFormulaire (): void {
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);
        $utilisateur->ajouter();
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
        self::afficherVue('vueGenerale.php', ["titre" => "Liste des utilisateurs","cheminCorpsVue" => "utilisateur/utilisateurCree.php",'utilisateurs' => $utilisateurs]);

    }
}
?>
