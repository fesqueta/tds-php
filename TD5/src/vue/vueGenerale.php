<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="../ressources/css/navstyle.css">
    <meta charset="UTF-8">
    <?php
    /**
    * @var string $titre
    */
    ?>
    <title><?php echo $titre; ?></title>

</head>
<body>
<header>
    <nav>
        <ul>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li><li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li>
        </ul>
    </nav>
</header>
<main>
    <?php
    /**
    * @var string $cheminCorpsVue
    */
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de Adrien Fesquet
    </p>
</footer>
</body>
</html>

