<?php

namespace App\Covoiturage\Modele;

class ModeleUtilisateur
{
    private String $login;
    private String $nom;
    private String $prenom;

    // un getter
    public function getNom() : String{
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function getLogin() : String
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin(String $login)
    {
        $this->login = substr($login, 0, 64);
    }

    /**
     * @return mixed
     */
    public function getPrenom() : String
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom(String $prenom)
    {
        $this->prenom = $prenom;
    }


    // un setter
    public function setNom(String $nom) {
        $this->nom = $nom;
    }

    // un constructeur
    public function __construct(
        String $login,
        String $nom,
        String $prenom
    ) {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
   /* public function __toString() :String {
        // À compléter dans le prochain exercice
        return "$this->login<br> $this->nom<br> $this->prenom <br>";
    }*/

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : ModeleUtilisateur {
        return new ModeleUtilisateur($utilisateurFormatTableau['loginBaseDeDonnees'],
        $utilisateurFormatTableau['nomBaseDeDonnees'],
        $utilisateurFormatTableau['prenomBaseDeDonnees']);
    }

    public static function recupererUtilisateurs(): array {
        $pdostatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM utilisateur");
        $utilisateursFormatTableau = $pdostatement->fetchAll();
        $utilisateurs = [];
        foreach ($utilisateursFormatTableau as $utilisateurTableau) {
            $utilisateur = self::construireDepuisTableauSQL($utilisateurTableau);
            $utilisateurs[] = $utilisateur;
        }
        return $utilisateurs;
        }
    public static function recupererUtilisateurParLogin(string $login) : ?ModeleUtilisateur {
        $sql = "SELECT * from utilisateur WHERE loginBaseDeDonnees = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();

        if($utilisateurFormatTableau == null){return null;}

        return ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter() : void {

        $sql = "INSERT INTO utilisateur VALUES (:loginTag, :nomTag, :prenomTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "loginTag" => $this->login,
            "nomTag" => $this->nom,
            "prenomTag" => $this->prenom,

        );
        $pdoStatement->execute($values);

    }


}