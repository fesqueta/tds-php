<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        <p>
            <?php
            // Ceci est un commentaire PHP sur une ligne
            /* Ceci est le 2ème type de commentaire PHP
            sur plusieurs lignes */

            // On met la chaine de caractères "hello" dans la variable 'texte'
            // Les noms de variable commencent par $ en PHP


            // On écrit le contenu de la variable 'texte' dans la page Web


            $utilisateur1 = [
                    'nom' => 'fesquet',
                    'prenom' => 'adrien',
                    'login' => 'fesqueta'
            ];
            /*foreach ($utilisateur1 as $key => $value) {
                if($key == 'login') {
                    echo "de login ". $value;
                }
                else echo $value. " ";

            }*/
            echo "<br>";
            $utilisateur2 = [
                'nom' => 'fesquet',
                'prenom' => 'tom',
                'login' => 'fesquett'
            ];
            $utilisateur3 = [
                'nom' => 'desmartes',
                'prenom' => 'florian',
                'login' => 'desmartesf'
            ];


            //$utilisateurs = [$utilisateur1, $utilisateur2, $utilisateur3];
            $utilisateurs = [];
            echo "Liste des utilisateurs :";
            if(sizeof($utilisateurs)==0) { echo "Il n'y a aucun utilisateur";}
            else {
                echo "<ul>";
                for ($i = 0; $i < count($utilisateurs); $i++) {
                    echo "<li>";
                    foreach ($utilisateurs[$i] as $key => $value) {
                        echo $value . " ";
                    }
                    echo "</li>";
                }
                echo "</ul>";
            }
            ?>
        </p>

    </body>
</html> 