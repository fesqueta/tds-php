<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs,"titre" => "Liste des utilisateurs","cheminCorpsVue" => "utilisateur/liste.php"]);
    }
    public static function afficherDetail() : void {
        $login = $_GET['login'];
        if($login == NULL || $login == ""){
            //require ('../vue/utilisateur/erreur.php');
            self::afficherErreur( 'le login n\'existe pas');

        return;}
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        if($utilisateur == NULL){
            //require ('../vue/utilisateur/erreur.php');
            self::afficherErreur( 'l\'utilisateur  n\'existe pas');

            return;}
        //require ('../vue/utilisateur/detail.php');
        self::afficherVue('vueGenerale.php', ['utilisateur' => $utilisateur,"titre" => "Detail de l'utilisateur","cheminCorpsVue" => "utilisateur/detail.php"]);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__. "/../../src/vue/$cheminVue"; // Charge la vue
    }
    public static function afficherFormulaireCreation (): void {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire de création","cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }
    public static function creerDepuisFormulaire (): void {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->ajouter($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["titre" => "Liste des utilisateurs","cheminCorpsVue" => "utilisateur/utilisateurCree.php",'utilisateurs' => $utilisateurs]);

    }
    public static function afficherErreur(string $messageErreur = ""): void {
        self::afficherVue('vueGenerale.php', ["titre" => "Erreur utilisateur","cheminCorpsVue" => "utilisateur/erreur.php",'erreur' => $messageErreur]);
    }
    public static function supprimer (): void {
        $login = $_GET['login'];
        if($login == NULL || $login == ""){
            self::afficherErreur( 'le login n\'existe pas');

            return;}
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        if($utilisateur == NULL){
            //require ('../vue/utilisateur/erreur.php');
            self::afficherErreur( 'l\'utilisateur  n\'existe pas');

            return;}
        (new UtilisateurRepository())->supprimer($login);
        $utilisateurs = (new UtilisateurRepository())->recuperer();

        //require ('../vue/utilisateur/detail.php');
        self::afficherVue('vueGenerale.php', ['login' => $login,"titre" => "Supprimer l'utilisateur","cheminCorpsVue" => "utilisateur/utilisateurSupprime.php",'utilisateurs' => $utilisateurs]);
    }
    public static function afficherFormulaireMiseAJour ():void {
        $login = $_GET['login'];
        self::afficherVue('vueGenerale.php', ['login' => $login,"titre" => "Afficher le formulaire de mise a jour","cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php"]);
    }
    public static function verificationUtilisateur ($login) : bool {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if($utilisateur == NULL){
            self::afficherErreur( 'le login  n\'existe pas');
            return false;
        }
        return true;
    }
    public static function mettreAJour ():void {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        if(self::verificationUtilisateur($utilisateur->getLogin())){
            (new UtilisateurRepository())->mettreAJour($utilisateur);
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            self::afficherVue('vueGenerale.php',["cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php",'login' => $login,'utilisateurs' => $utilisateurs]);

        }

    }

    /**
     * @return Utilisateur
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $login = $tableauDonneesFormulaire['login'];
        $nom = $tableauDonneesFormulaire['nom'];
        $prenom = $tableauDonneesFormulaire['prenom'];
        $utilisateur = new Utilisateur($login, $nom, $prenom);
        return $utilisateur;
    }
}
?>
