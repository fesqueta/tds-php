<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class ControleurTrajet {

    public static function afficherListe() : void {
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ['trajets' => $trajets,"titre" => "Liste des trajets","cheminCorpsVue" => "trajet/liste.php"]);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__. "/../../src/vue/$cheminVue"; // Charge la vue
    }

    public static function afficherErreur(string $messageErreur = ""): void {
        self::afficherVue('vueGenerale.php', ["titre" => "Erreur de trajet","cheminCorpsVue" => "trajet/erreur.php",'erreur' => $messageErreur]);
    }
    public static function afficherDetail() : void {
        $id = $_GET['id'];
        if($id == NULL || $id == ""){
            //require ('../vue/utilisateur/erreur.php');
            self::afficherErreur( 'l\'id n\'existe pas');

            return;}
        $trajet = (new TrajetRepository())->recupererParClePrimaire($_GET['id']);
        if($trajet == NULL){
            //require ('../vue/utilisateur/erreur.php');
            self::afficherErreur( 'l\'id  n\'existe pas');

            return;}
        self::afficherVue('vueGenerale.php', ['trajet' => $trajet,"titre" => "Detail du trajet","cheminCorpsVue" => "trajet/detail.php"]);
    }
    public static function supprimer (): void
    {
        $id = $_GET['id'];
        if($id == NULL || $id == ""){
            self::afficherErreur( 'l\'id n\'existe pas');
            return;
        }
        $trajet = (new TrajetRepository())->recupererParClePrimaire($_GET['id']);
        if($trajet == NULL){
            //require ('../vue/utilisateur/erreur.php');
            self::afficherErreur( 'le trajet  n\'existe pas');

            return;}
        (new TrajetRepository())->supprimer($id);
        $trajets = (new TrajetRepository())->recuperer();

        //require ('../vue/utilisateur/detail.php');
        self::afficherVue('vueGenerale.php', ['id' => $id,"titre" => "Supprimer le trajet","cheminCorpsVue" => "trajet/trajetSupprime.php",'trajets' => $trajets]);

    }
    public static function afficherFormulaireCreation ():void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire de création","cheminCorpsVue" => "trajet/formulaireCreation.php"]);

    }

    /**
     * @throws \DateMalformedStringException
     */
    public static function creerDepuisFormulaire():void{

        $trajet = self::construireDepuisFormulaire();
        (new TrajetRepository())->ajouter($trajet);
        echo "Le trajet à bien été crée";
    }
    public static function afficherFormulaireMiseAJour ():void {
        $id = $_GET['id'];
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        self::afficherVue('vueGenerale.php', ['trajet' => $trajet,"titre" => "Afficher le formulaire de mise a jour","cheminCorpsVue" => "trajet/formulaireMiseAJour.php"]);
    }

    /**
     * @return Trajet
     * @throws \DateMalformedStringException
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $date = new DateTime($tableauDonneesFormulaire["date"]);
        $login = $tableauDonneesFormulaire["conducteurLogin"];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        $trajet = new Trajet($tableauDonneesFormulaire["id"] ?? null, $tableauDonneesFormulaire['depart'],
            $tableauDonneesFormulaire['arrivee'], $date, $tableauDonneesFormulaire['prix']
            , $utilisateur, isset($tableauDonneesFormulaire["nonFumeur"]));
        return $trajet;
    }
    public static function mettreAJour ():void {
        $trajet = self::construireDepuisFormulaire($_GET);
        if(self::verificationTrajet($trajet->getId())){
            (new TrajetRepository())->mettreAJour($trajet);
            $trajets = (new TrajetRepository())->recuperer();
            self::afficherVue('vueGenerale.php',["cheminCorpsVue" => "trajet/trajetMisAJour.php",'trajets' => $trajets]);

        }

    }
    public static function verificationTrajet ($id) : bool {
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        if($trajet == NULL){
            self::afficherErreur( 'le login  n\'existe pas');
            return false;
        }
        return true;
    }

}