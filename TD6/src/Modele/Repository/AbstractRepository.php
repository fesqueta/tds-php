<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;

abstract class AbstractRepository
{
    public function mettreAJour(AbstractDataObject $object): void
    {
        $colonnes = $this->getNomsColonnes();
        $tabFormaté = [];
        foreach ($colonnes as $colonne) {
                $tabFormaté[] = "$colonne = :{$colonne}Tag";
            }

        $sql = "UPDATE ". $this->getNomTable() ." SET ". join(", ",$tabFormaté)
            . " WHERE " . $this->getNomClePrimaire() ." = :".$this->getNomClePrimaire()."Tag";
        //$sql = "UPDATE utilisateur SET loginBaseDeDonnees = :loginTag, nomBaseDeDonnees = :nomTag, prenomBaseDeDonnees = :prenomTag
        //        WHERE loginBaseDeDonnees = :loginTag";
        //")";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = $this->formatTableauSQL($object);
        $pdoStatement->execute($values);
    }

    public function ajouter(AbstractDataObject $objet): bool
    {
        $sql = "INSERT INTO ". $this->getNomTable() ." (".join(",",$this->getNomsColonnes()). ") " ."VALUES (:".
        join("Tag, :",$this->getNomsColonnes()). "Tag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = $this->formatTableauSQL($objet);
        $pdoStatement->execute($values);
        return true;
    }
    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;

    protected abstract function getNomsColonnes(): array;

    public function supprimer($valeurClePrimaire): void
    {
        $sql = "DELETE FROM ". $this->getNomTable() ." WHERE ". $this->getNomClePrimaire() ." = :valeurClePrimaire";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "valeurClePrimaire" => $valeurClePrimaire
        );
        $pdoStatement->execute($values);
    }

    public function recupererParClePrimaire(string $clePrimaire): ?AbstractDataObject
    {
        $sql = "SELECT * from ". $this->getNomTable()." WHERE ". $this->getNomClePrimaire()." = :cleprimaireTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "cleprimaireTag" => $clePrimaire,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemmentss
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $objetFormatTableau = $pdoStatement->fetch();

        if ($objetFormatTableau == null) {
            return null;
        }

        return $this->construireDepuisTableauSQL($objetFormatTableau);
    }

    /**
     * @return AbstractDataObject[]
     */
    public function recuperer(): array
    {
        $sql = "SELECT * FROM " . $this->getNomTable();
        $pdostatement = ConnexionBaseDeDonnees::getPDO()->query($sql);
        $elementFormatTableau = $pdostatement->fetchAll();
        $elements = [];
        foreach ($elementFormatTableau as $elementTableau) {
            $element = $this->construireDepuisTableauSQL($elementTableau);
            $elements[] = $element;
        }
        return $elements;
    }

    protected abstract function getNomTable(): string;
    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;

    protected abstract function  getNomClePrimaire() : string;



}