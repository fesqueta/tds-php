<?php

namespace App\Covoiturage\Modele\DataObject;

class Utilisateur extends AbstractDataObject
{
    private String $login;
    private String $nom;
    private String $prenom;

    // un getter
    public function getNom() : String{
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function getLogin() : String
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin(String $login)
    {
        $this->login = substr($login, 0, 64);
    }

    /**
     * @return mixed
     */
    public function getPrenom() : String
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom(String $prenom)
    {
        $this->prenom = $prenom;
    }


    // un setter
    public function setNom(String $nom) {
        $this->nom = $nom;
    }

    // un constructeur
    public function __construct(
        String $login,
        String $nom,
        String $prenom
    ) {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
   /* public function __toString() :String {
        // À compléter dans le prochain exercice
        return "$this->login<br> $this->nom<br> $this->prenom <br>";
    }*/




}