<!DOCTYPE html>
<html>
<meta charset="utf-8" />
<title> Formulaire mise à jour trajet</title>
<?php
use App\Covoiturage\Modele\DataObject\Trajet;
/** @var Trajet $trajet */
?>
<body>
<form method="get" action="controleurFrontal.php">
    <input type='hidden' name='action' value='mettreAJour'>
    <input type="hidden" name='controleur' value='trajet'>
    <fieldset>
        <legend>Mon formulaire de modification:</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="id_id">Id&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= $trajet->getId() ?>" name="id" id="id_id" required readonly="readonly">
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="depart_id">Depart&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($trajet->getDepart()) ?>" name="depart" id="depart_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="arrivee_id">ArrivÃ©e&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($trajet->getArrivee()) ?>" name="arrivee" id="arrivee_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="date_id">Date&#42;</label>
            <input class="InputAddOn-field" type="date" value="<?= $trajet->getDate()->format("Y-m-d") ?>" name="date" id="date_id"  required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prix_id">Prix&#42;</label>
            <input class="InputAddOn-field" type="int" value="<?= $trajet->getPrix() ?>" name="prix" id="prix_id"  required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="conducteurLogin_id">Login du conducteur&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= $trajet->getConducteur()->getLogin() ?>" name="conducteurLogin" id="conducteurLogin_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nonFumeur_id">Non Fumeur ?</label>
            <input class="InputAddOn-field" type="checkbox" checked="<?= $trajet->isNonFumeur() ?>" name="nonFumeur" id="nonFumeur_id"/>
        </p>
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>


</body>
</html>