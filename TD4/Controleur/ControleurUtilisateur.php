<?php
require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        //require ('../vue/utilisateur/liste.php');  //"redirige" vers la vue
        self::afficherVue('utilisateur/liste.php', ['utilisateurs' => $utilisateurs]);
    }
    public static function afficherDetail() : void {
        $login = $_GET['login'];
        if($login == NULL || $login == ""){
            //require ('../vue/utilisateur/erreur.php');
            self::afficherVue('utilisateur/erreur.php',[]);

        return;}
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login']);
        if($utilisateur == NULL){
            //require ('../vue/utilisateur/erreur.php');
            self::afficherVue('utilisateur/erreur.php',[]);
        return;}
        //require ('../vue/utilisateur/detail.php');
        self::afficherVue('utilisateur/detail.php', ['utilisateur' => $utilisateur]);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }
    public static function afficherFormulaireCreation (): void {
        self::afficherVue('utilisateur/formulaireCreation.php', []);
    }
    public static function creerDepuisFormulaire (): void {
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);
        $utilisateur->ajouter();
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
        self::afficherVue('utilisateur/liste.php', ['utilisateurs' => $utilisateurs]);
    }


}
?>
